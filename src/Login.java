
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Button;

public class Login extends Application {
  
  private static final String USER = "snake";
  private static final char[] PASS = {'s', 'n', 'a', 'k', 'e'};

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage mainMenu) throws Exception {
    mainMenu.setTitle("Snake");
    
    GridPane mainMenuLayout = new GridPane();
    mainMenuLayout.setAlignment(Pos.CENTER);
    mainMenuLayout.setHgap(10);
    mainMenuLayout.setVgap(10);
    mainMenuLayout.setPadding(new Insets(25, 25, 25, 25));
    
    Scene scene = new Scene(mainMenuLayout);
    scene.getStylesheets().add(Login.class.getResource("Login.css").toExternalForm());
    mainMenu.setScene(scene);
    
    Text sceneTitle = new Text("Snake");
    sceneTitle.setId("welcome-text");
    sceneTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
    mainMenuLayout.add(sceneTitle, 0, 0, 2, 1);
    
    Label userName = new Label("User Name:");
    mainMenuLayout.add(userName, 0, 1);
    
    TextField userTextField = new TextField();
    mainMenuLayout.add(userTextField, 1, 1);
    
    Label pw = new Label("Password:");
    mainMenuLayout.add(pw, 0, 2);
    
    PasswordField pwBox = new PasswordField();
    mainMenuLayout.add(pwBox, 1, 2);
    
    Button signInBtn = new Button("Sign in");
    HBox hbBtn = new HBox(10);
    hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
    hbBtn.getChildren().add(signInBtn);
    mainMenuLayout.add(hbBtn, 1, 4);
    
    final Text signInMessage = new Text();
    signInMessage.setId("sign-in-msg");
    mainMenuLayout.add(signInMessage, 1, 6);
    
    signInBtn.setOnAction(new EventHandler<ActionEvent>() {
      
      @Override
      public void handle(ActionEvent e) {
        // Successful login
        if(pwBox.getText().equals(new String(PASS)) && userTextField.getText().equals(USER)) {
          mainMenu.close();
          (new Game()).start();
          
        // Unsuccessful Login
        } else {
          signInMessage.setText("          Incorrect credentials");
        }
      }
      
    });
    
    mainMenu.show();
  }
  
  
}
